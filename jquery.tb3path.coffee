do ($ = jQuery) ->
	class tb3Path
		constructor : (element, options) ->
			@element = $ element
			@options = $.extend {}, $.fn.tb3path.defaults, options
			@init()
			_ = @
			@element.on 'click', '.icon-remove', () ->
				$(@).parent().remove()
				_.render()
		init : () ->
			@element.html '<div class="wizard wizard-path"><input type="text" style="width:100%;right:0px;position:absolute;height:100%;" class="form-control input-path"><ul class="steps"></ul></div>'
			@steps = @element.find 'ul.steps'
			@input = @element.find '.input-path'
			_ = @

			@input.keyup (event) =>
				if event.which is 13
					if @options['direct'] == 'append' then _.append() else _.prepend()
		append : (string) ->
			if string is undefined
				string = @input.val()
				@input.val ''
			else
				string = ''+string

			if string.length > 0
				@steps.append "<li class='complete li-path'><i class='glyphicon glyphicon-remove-circle icon-remove'></i>  <span class='value-path-part'>#{string}</span><span class='chevron'></span></li>"
				@render()
		prepend : (string) ->
			if string is undefined
				string = @input.val()
				@input.val ''
			else
				string = ''+string

			if string.length > 0
				@steps.prepend "<li class='complete li-path'><i class='glyphicon glyphicon-remove-circle icon-remove'></i>  <span class='value-path-part'>#{string}</span><span class='chevron'></span></li>"
				@render()
		render : () ->
			heightElement = @element.width();
			heightLi = 0
			li = $ '.steps > li'
			li.each () ->heightLi += $(@).width()+50
			@input.css {'width':(if li.length > 0 then heightElement-heightLi+7 else heightElement)+'px'}
		remove : (idx) ->
			el = @element.find('.li-path').eq(idx)
			if el
				el.remove()
				@element.trigger 'change'
				@render()


	jQuery.fn.tb3path = (option,value) ->
		methodReturn = ''
		$set = @each ()->
			$this = $ this
			data = $this.data 'tb3path'
			options = typeof option is 'object' && option

			$this.data 'tb3path', data = new tb3Path this, options if not data 
			methodReturn = data[option](value) if typeof option is 'string'
		methodReturn is undefined ? $set : methodReturn;
	jQuery.fn.tb3path.defaults = {'direct':'append'}